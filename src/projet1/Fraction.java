package projet1;

public class Fraction {

	int num;
	int denum;
	
	public Fraction(){
		num = 0;
		denum = 1;
	}
	
	public Fraction(int n){
		num = n;
		denum = 1;
	}
	
	public Fraction(int n, int d){
		num = n;
		denum = d;
	}
	

	public String toString(){
		
		int n,d;
		
		n = this.num;
		d = this.denum;
		
		return String.valueOf(n)+"/"+String.valueOf(d);
	}
}
